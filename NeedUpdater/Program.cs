﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NeedUpdater
{
    static class Program
    {
        public static bool Enabled { get; private set; }

        /// <summary>
        /// Główny punkt wejścia dla aplikacji.
        /// </summary>
        [STAThread]
        static void Main()
        {

            if (Directory.Exists(@"C:\NeedScreen"))
            {

            }
            else
            {
                Directory.CreateDirectory(@"C:\NeedScreen");
            }
            WebClient wc = new WebClient();
            var completed = new AutoResetEvent(false);
            wc.DownloadFileAsync(new Uri("https://ci.appveyor.com/api/projects/bartixxx/screen/artifacts/Screenshotter/bin/Debug/Screenshotter.exe"), @"C:\NeedScreen\NeedScreen.exe");
            wc.DownloadProgressChanged += (s, ev) =>
            {

            };
            wc.DownloadFileCompleted += (s, ev) =>
            {
                //All the rest of the code 
                Process NeedScreen = new Process();
                Directory.SetCurrentDirectory(@"C:\NeedScreen");
                NeedScreen.StartInfo.FileName = @"C:\NeedScreen\NeedScreen.exe";
                NeedScreen.Start();
                System.Timers.Timer timer1 = new System.Timers.Timer();
                timer1.Interval = 600000;
                timer1.Elapsed += timer_Elapsed;
                timer1.Start();
            };
            completed.WaitOne();

        }
        static void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string md5_string = "";
            string remote_string = "";
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead("NeedScreen.exe"))
                {
                    byte[] md5_result = md5.ComputeHash(stream);
                    md5_string = BitConverter.ToString(md5_result).Replace("-", "").ToLower();
                    var remote_result = (new WebClient()).DownloadString("https://ci.appveyor.com/api/projects/Bartixxx/screen/artifacts/Screenshotter%2Fbin%2FDebug%2Fmd5.txt");
                    remote_result = remote_result.Replace(" ", "").ToLower();
                    string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    Directory.SetCurrentDirectory(exeDir);

                    if (md5_string == remote_result)
                    {
                    }
                    else
                    {
                        foreach (var process in Process.GetProcessesByName("NeedScreen"))
                        {
                            process.Kill();
                            Thread.Sleep(5000);
                            Directory.SetCurrentDirectory(@"C:\NeedScreen");
                            Process NeedUpdater = new Process();
                            NeedUpdater.StartInfo.FileName = @"NeedScreenUpdater.exe";
                            NeedUpdater.Start();
                            Application.Exit();
                        }

                    }


                }
            }
            
        }

    }
}
